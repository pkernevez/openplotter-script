# Introduction
This documentation explains how I setup the [OpenPlotter](http://sailoog.com/openplotter) distribution
to use a Raspberry PI 3 as a full onboard computer.

[OpenPlotter](http://sailoog.com/openplotter) package and integrate several open source tools.
Some of them are : [OpenCPN](https://opencpn.org/) (navigation software), [ZyGrib](http://www.zygrib.org) (weather),
[SignalK](http://signalk.org/) (open data format exchange) and a lots of pre-installed configuration for sensors.

That's what he looks like :

![pi](docs/images/installation.jpeg)

The main functions that are setup on my configuration :
* Basic Raspberry PI 3 features : HDMI, Bluetooth, Wifi
* [OpenCPN](https://opencpn.org/) for navigation
* [ZyGrib](http://www.zygrib.org) for weater (required internet access)
* Integration with the NMEA 0183 boat bus (get data from GPS, AIS and
all the classical electronic instruments of the boat)
* Accelerator sensors (magnetic compas, heel and pitch)
* Weather sensors (pressure, temperature and humidity)
* Additional temperature sensors (with high robustness) for motor, fridge or sea.
* Buzzer for on board alerts (with a simple diode that allow to test alert at home)
* Wireless keyboard and mouse
* GPS through USB (it's a backup of the boat GPS and allow testing at home).
It's also possible to use an iphone as backup solution with the good free applicatiob
[GPS2IP](https://itunes.apple.com/us/app/gps-2-ip/id408625926).
* Additional USB WIFI card : this configuartion allow to have at the same time a fix access point
for the PI (the navigation network) and to be connected to another wifi (Home wifi or mobile access point)
to have an internet connection, necessary for weather download or updates.
* Sharing of all the bus information (GPS, instruments, etc.) on a remote computer and iPad
(with Navionics application).

Next features I want to setup :
* Setup of a SDR (Software Defined Radio) for receiving Weather Fax in short waves (BLU) and having
an AIS in backup (reception only).
* Man overboard system with BlueTooth bracelets (BLE), similar to the principe of
[sea tags](https://sea-tags.com) but that will use the Raspberry PI instead of a phone (I prefer to rely
on the Raspberry PI instead of an iphone when sailing offshore).
* 3D printing enclosure like https://github.com/mathieubrun/raspberry-enclosure

## Detail of my installation

[Installation](docs/installation.md)

## Setup of an anchor alarm

[anchor alarm](docs/anchor_alarm.md)


QQQ Setup alarm during cruise : lost of NMEA message
https://opencpn.org/wiki/dokuwiki/doku.php?id=opencpn:opencpn_user_manual:advanced_features:nmea_sentences
* GLL : GPS Latitude/longitude
* VTG : groud speed
* AIVDM/AIVDO : AIS
* MWD/MWV : Wind speed

## Use QtVlm for routing

[QtVlm configuration and usage](docs/qtvlm.md)




