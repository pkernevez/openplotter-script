# Alarme de mouillage

## Motivation
Les applications pour téléphone ne m'ont pas donné satisfaction : manque de configuration et
limitations de la précision du GPS à l'interieur du bateau. En plus, je n'aime pas dépendre
d'un téléphone et de sa batterie en navigation.

Il y a un plugin OpenCPN ([Watchdog](https://opencpn.org/wiki/dokuwiki/doku.php?id=opencpn:opencpn_user_manual:plugins:safety:watchdog))
qui fait parfaitement le job à un point près : il est prévu
pour des alertes visuelles ou sonores (via la carte son), or mon pi n'est pas relié à des enceintes.

Cette configuration permet d'allumer le buzzer pour notifier les alertes de watchdog.

Watchdog permet de faire plus que de simple alertes pour le mouillage, mais ce sont les seuls que j'ai testée.

Le buzzer est connecté sur la GPIO 17 (qui correspond à la PIN 11 de la carte).

Dans mon installation, lorsque Watchdog lève une alerte, le buzzer fait des bip (0.5s de buz, 0.5s de silence)
pendant 15s, puis fait une pause de 5s et recommence si l'alerte est toujours valide.

## Installation du script qui lance le buzzer
### Solution 1 la plus simple si le PI est connecté à internet
Cloner mon repository dans /home/pi/script et donner les droits d'exécution au fichier en lançant
les commandes suivantes depuis le `Terminal`.
```
git clone https://gitlab.com/pkernevez/openplotter-script.git /home/pi/script
```

### Alternative en éditant le fichier à la main
Créer le fichier `/home/pi/script/buzzer.py` via l'utilitaire `TextEditor` disponible dans le menu accessoire
du PI.
Copier le contenu du fichier [buzzer.py](../scripts/buzzer.py).

### Alternative en copiant le fichier depuis votre ordinateur
Utiliser un logiciel comme [cyberduck](https://cyberduck.io/) pour copier le fichier [buzzer.py](../scripts/buzzer.py) dans le répertoire `/home/pi/script`.

## Configuration du script
Il y a 2 variables à changer éventuellement :
```
BuzzPin = 11    # Raspberry Pi Pin 17-GPIO 17
NbSeconds = 15  # Duration of the beeps in seconds
```

## Tester le script
Sur le pi lancer un "terminal" via le menu accessoire et taper la commande :
```
python /home/pi/script/buzzer.py
```
Le buzzer doit sonner pendant 15s et un message de ce type doit s'afficher dans le terminal :
```
2019-01-13 22:37:52,099 - INFO - Setup on PIN : 11
2019-01-13 22:38:07,118 - INFO - Clean GPIO
```



## Configurer OpenCPN
### Watchdog
* activer le plugin WatchDog
* Lancer sa configuration

![watchdog](images/watchdog.png)
* ajouter une alarme de mouillage

![newalram](images/newalarm.png)

* configurer l'alarm en veillant à :
  - Choisir la sensibilité de l'alerte voulue (50m par défaut)
  - Cocher 'Automatically synchronize to the boat whenever enabled' => chaque fois qu'on réactivera la rêgle, la position courante sera utilisée pour localiser l'ancre
  - Cocher commande et mettre la valeur ```python /home/pi/script/buzzer.py``` pour que l'alerte déclenche le buzzer
  - Cocher "Repeat alert after" et mettre 20 secondes (l'alert sonne 15s, marque une pause de 5s seconde et recommence jusqu'à une intervention
humaine ou que le bateau revienne dans le cercle de l'alarme
  - Ne pas cocher "Message Box" cela empêche de relancer le buzzer au bout de 20s tant que personne n'a cliqué sur le message d'alerte
Plus d'information sur la cofiguration [ici](https://opencpn.org/wiki/dokuwiki/doku.php?id=opencpn:opencpn_user_manual:plugins:safety:watchdog#anchor_alarm)

Cliquer sur 'Test', pour vérifier que le buzzer se déclenche bien.

![confalarm](images/configurealarm.png)

Et voila... Maintenant si Watchdog est lancé, chaque fois qu'on active l'alerte (en cochant la checkbox), la position de l'ancre est réinitilisée
avec la position courante et ça sonne si on sort du cercle.

