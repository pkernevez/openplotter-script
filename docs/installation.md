# Motivation
There is a more ready to use Raspberry PI with a Hat (
[Moitessier Hat](https://shop.sailoog.com/openplotter/4-moitessier-hat.html) ), I've prefered
setup mine by myself.

# Overview

![pi](images/installation.jpeg)

This documentation has been updated with Raspberry PI 4. The documentation for the previous version is [Rapsberry PI 3](https://gitlab.com/pkernevez/openplotter-script/-/tree/Raspberry-PI-3).

# Assembly
![assembly](images/assembly.jpg)

![cabling](images/cablage.png)

# My components
Notes : I do not sell any of those components, this is only the list I bought

* 12V power supply for the PI (an USB plug on a car lighter is also a good supply)
[on Amazon](https://www.amazon.com/gp/product/B00U2DGJD2/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)
* Batterie (usefull at home or in another boat), a full day of autonomy with 72Wh
[in a classical shop](https://www.cellularline.com/xc_en/products/freepower-20000-universale.html)
* Acceleration and magnetic sensors (magnetic compas, heel and pitch) : MPU 9255
[on Amazon](https://www.amazon.com/gp/product/B01DIGRR8U/ref=oh_aui_detailpage_o03_s02?ie=UTF8&psc=1)
(pay attention to the location of the buzzer, it contains a magnet that interferes with the magnetic compas)
* Weather sensors (air pressure, temperature and humidity) : BME280
[on Amazon](https://www.amazon.com/gp/product/B0775HR32R/ref=oh_aui_detailpage_o03_s01?ie=UTF8&psc=1)
(pay attention to the location of this sensor : protected in a hermetic box with the PI it will more indicate
the PI temperature than the room temperature.
* Additional temperature sensor (with high robustness) for motor, fridge or sea : sensors DS18B20 (-20°C to 200°C)
[on Amazon](https://www.amazon.com/gp/product/B00Q9YBIJI/ref=oh_aui_detailpage_o03_s02?ie=UTF8&psc=1)
* Additional Wifi USB card (the first one is integrated in the PI board)
[on Amazon](https://www.amazon.com/gp/product/B003MTTJOY/ref=oh_aui_detailpage_o03_s01?ie=UTF8&psc=1)
* NMEA 0183 reception: RS422/485 serial port on USB
[on Amazon](https://www.amazon.com/gp/product/B076WVFXN8/ref=od_aui_detailpages02?ie=UTF8&psc=1)
* Actif Buzzer 5V (need also a diode, a resistance and a transistor)
[on Amazon](https://www.amazon.com/gp/product/B073RH8TQK/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)
As they are not enough noisy I'd just ordered new ones [95dB buzzer](https://www.amazon.com/gp/product/B0167X51E8/ref=ppx_od_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
* 15" Screen 8W with 12V power supply and HDMI connection
[It should be more or less this model](https://www.uship.fr/default/ecran-lcd-12-v-11320.html)

# How did I my setup
##. Download and installation of OpenPlotter image 1.0.0 (I'm on a MAC)
1. Start ['SD Card Formater'](https://www.sdcard.org) to format a new card
(quick format, [avoid card with more than 64GB](https://docs.sailoog.com/openplotter-v1-x-x/getting-started).
2. Download the zip file [NOOBS](https://archive.org/details/openplotter_v1.0.0_noobs)
3. Transfer all the extract files on the new formatted card

##. Boot on the new card with a screen, keyboard and mouse (you can't be headless for this step) :
1. Setup Wifi
    1. Click on the wifi icone (top right of the screen) to configure the 2nd Wifi card (if you have one) on a known wifi
    1. Name of my network interfaces :
        1. WLAN1 : builtin card
        2. WLAN0 : Wifi USB Card
        3. Eth0 : cable
    2. Start OpenPlotter check the configuration
        1. Onglet : Network, configuration
            1. Networkmode : RPi3+: AP + Client
            2. Sharing interface : WLAN0
2. In the RasperryPI configuration` menu
    1. Click on the Raspberry icone (top left of the screen) to start `Raspberry configuration`
    2. Change the default password of the `pi` account : _YourPIPassword_
    3. Activate (Interface``mnenu) SSH
    4. Increase the GPU memory to 128Mo
    5. Set timezone and Wifi Country
3. Into OpenPlotter
    1. Start OpenPlotter``
    2. On the Wifi tab, change the secret code for the OpenPlotter network : _YourWifiPassword_
    3. Reboot if necessary
4. If you have a 2nd wifi card, your can write down its IP address. You can use it to connect remotely from another computer and keep the computer's internet connection
(if your connect headless through the `open plotter` network you won't keep access to internet at the same time)
    1. Your can click on the Wifi icon
    2. Or run a `terminal` and run the command `ifconfig`
You should have the following configuration :
```
> cat /etc/network/interfaces.d/station
allow-hotplug wlan0
auto wlan0

iface wlan0 inet dhcp
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```
If you prefer to define a static IP Adress (advanced users only), you should define something like that
```
> cat /etc/network/interfaces.d/station
allow-hotplug wlan0
iface wlan0 inet static
        address 10.1.1.31
        netmask 255.255.255.0
        gateway 10.1.1.1
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```
    
5. We now can connect remotely to the PI with VNC or SSH. We won't need any more a screen or a keybord (except in case of misconfiguration of the network),
this is useful when working on the PI at home.
6. Copy your SSH key into the `pi` account (if you have a ssh key)
    1. `ssh-copy-id pi@$IP` where $IP should be replaced by _10.10.10.1_ if you are using the `open plotter` acess point or
     the previous IP address of the 2nd Wifi card.
7. Signal K configuration
    1. Open a browser and go to `http://localhost:3000/admin`
    2. Use the pi account for login
    2. Into the security menu change the user/password : _openplotter / YourPassword_
8. Update Openplotter & co to the last update:
    1. Start `open plotter` and open the menu `update`
    2. Update OpenPlotter, then reboot
    3. Update OpenCPN
    4. Update OpenCPN-plugins
9. Advanced users : VIM compatibility mode for support of the arrow key when editing file (insert mode)
```
echo "set nocp" >> ~/.vimrc
sudo sh -c 'echo "set nocp" >> /root/.vimrc'
``` 

## Screen resolution:
Your can change the screen resolution if needed. Go to the PI menu : _Preferences > PI Configuration_ then choose _Set Resolution_ : 1280 x 720 (or your chosen resolution)

## Cards
1. You can copy your cards to the relevant folder :
    * rsync -avz _YourLocalCardFolder_/* pi@openplotter2.local://home/pi/Charts/
2. Configure the card into `open CPN` (see its documentation)

## Configure OpenPlotter for all the sensors:
1. Tab _Serial_ (GPS) :
    1. Connect the GPS to the USB port, then reboot
    2. Start `open plotter`, go to `Serial` tab
    3. It should see the GPS in the list device, the define the configuration:
        1. select ‘remember device’ (you won't have change if you plug the GPS into another port later)
        2. Choose a name: `tty_OP_gp`
        3. Assign it to `KPlex`

![GPS Configuration](images/OpenPlotter_GPS.png)

2. Tab _Kplex_ // TODO => broken at the moment => only the message of KPlex (GPS + weather)
        1. Add a broadcast UDP on the port 2000 for the Acvess point Wifi
            1. UDP out, 10.10.10.255, port 2000, no filtering

3. Tab _pypilot_ (Magnetic Compas, Heel and Pitch)
    1. Choose _IMU_Only_ as mode for _pylot_
    2. Select "Calibration" (and wait for 20-30s until the windows open :-(  ) and do... the calibration (follow the instruction)
    3. Select the 2 checkbox for _headingMagnetic_ and _pitch/roll_

    ![configuration](images/OpenPlotter_Pypilot.png)

    4. Click on ‘Apply Change’, your should see this IMU message into the _SKDiagnostic_ window if you open it.

    ![test](images/OpenPlotter_SignalK1.png)

3. Tab _I2C_ (pressure, humidity & temperature)
    1. Add the sensor _BME280_ (button _Add_).
    // TODO : check if inside ou outside...
    If you want to display this temperature in _OpenCPN_ in the part _'Air Temperature'_ like me, you'll need to change the key. OpenCPN is only able to display the temperature associated with the
    key _environnement.outside.temperature_, so replace the key _environnement.inside.temperature_ by the key _environnement.outside.temperature_.
    You may prefer to display another temperature sensor (see bellow the part _1W_), in this cas keep the existing key and use _environnement.outside.temperature_ for
    the other temperature sensor.
    An _1W_ temparture sensor may be more accurate as the temperature sensor _BME280_ is probably close to the _PI_ and located in a closed and protected box (it's temperature
     should be more the _PI_ temprature than the air temperature).

    ![test](images/OpenPlotter_I2C.png)

    2. Test with SKDiagnostic

    ![test](images/OpenPlotter_SignalK2.png)

    Nota bene : the pressure sensor is not supposed to be calibrated, mine displays 922 hPa, but this is normal I'm not living close to the sea (900m elevation).

4. Tab 1W ( for the _DS18B20_ temperature sensors)
    1. Connect the sensors
    2. Add a sensor (button _Add_), the Id proposed depending on the connected sensors. Use the the key _environnement.outside.temperature_ if the current sensor is
    the one you want to display in _OpenCPN_.

    ![test](images/OpenPlotter_1W.png)

    ![test](images/OpenPlotter_1WBis.png)

    Add all the sensors like this (1 to 3 sensors)

    3. Test with SKDiagnostic

5. Onglet SignalK (pour router les messages NMEA)
    Dans cette partie nous allons faire 2 choses : configurer SignalK pour recevoir les messages NMEA recçu
    via le convertisseur RS422/485 to USB et faire en sorte que tous les messages de SignalK soient publiés
    en UDP sur l'access point Wifi _openplotter_. Ce dernier point permettra à tous appareil connecté (autre ordinateur avec OpenCPN
    ou tablette avec un logiciel de navigation type Navionics) de recevoir les informations des instruments du bateau.
    1. Aller dans [SignalK](http://openplotter2.local:3000/admin/#/serverConfiguration/plugins) (avec openplotter / _VotreMotDePassePi_)
    2. Activer ;
        1. XDR (Barometer) => capteur de pression
        2. XDR (TempAir) => Air temperature

udp-nmea-plugin

1. Onglet Startup : Open OpenCPN Full Screen
OpenCPN se lancera automaziquement en plein écran à chaque démarrage. Pour sortir du mode plein écran il faut cliquer avec le bouton droit n'importe où sur la carte.


QQQ To continue
7. OpenCPN
    1. Activer le plugin dashboard, sont disponible
        1. True Compas (capteur IMU)
        2. Barometric Pressure (BME280)
        3. Pitch/Hell (ie pitch/roll) , (capteur IMU)
        4. Air Temp (reçoit environnement.outside.temperature)
8. Cloner script :
    1. git clone git@gitlab.com:pkernevez/openplotter-script.git ~/script
    2. Ou rsync : rsync -avz ~/Documents/SCM/Git/openplotter-script/ pi@openplotter.local:///home/pi/script/
9. Récupérer les exports (seulement pour export)
    1. scp -r "pi@openplotter2.local://home/pi/.node-red/lib/flows/*" /Users/pke/Documents/SCM/Git/openplotter-script/export
4.
5.
6.
4. Backup OpenCPN data
    1. scp -r "pi@openplotter2.local://home/pi/.opencpn/" /Users/pke/Documents/SCM/Git/openplotter-script


Under tests: 3D Model
![Box for PI4 and extension card](../3DModel/3d_example.png)

Your can use the file [U_Box_V103.scad](../3DModel/files/U_Box_V103.scad) with [OpenScad](https://openscad.org/).