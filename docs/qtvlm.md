# Why QtVlm ?
QtVlm is not an open source software, but it's a free software.

It has 2 features that doesn't have OpenCPN : capacity to download weather chart and 
the capacity to compute best route using the polar of the boat and the last weather.

OpenCPN has plugins to deal with those requirements, but honestly today 
they are not a the same level.

# Installation
Those installation has been tested with the version 5.8.9.

## Prequisites files :
### Documentation

[English documentation](http://download.meltemus.com/qtvlm/qtVlm_documentation_en.pdf)

[French documentation](http://download.meltemus.com/qtvlm/qtVlm_documentation_fr.pdf)


### Polars

To do routing you need to have the polar of you boat.
A lot of polars are available from [the Meltmenu site](http://download.meltemus.com/polars/), but also on 
[this drive](https://drive.google.com/drive/folders/1cRHRzDX0nMae2lPBe_bFYgijQH1tlK8w) for 
example the polar for [a Sun Legend 41 is here](http://download.meltemus.com/polars/Sunlegend41.pol).
 
If you don't find the polar of your boat you can use 
[OpenCPN and the polar plugin](https://opencpn.org/wiki/dokuwiki/doku.php?id=opencpn:opencpn_user_manual:plugins:sailing:polar) 
to create polar from you navigation (no explain here).

Copy the file to your PI with a tool like [CyberDuck](https://cyberduck.io/) to the folder `/home/pi/download`.
 
### Charts

If you have a PI that is not connected to Internet you need to download charts before the QtVlm installation (if your PI is connected, skip this step).
* [Download GSHHG](http://www.soest.hawaii.edu/pwessel/gshhg/gshhg-bin-2.3.7.zip)
* [Download GIS](resources/gis.zip)

Create a folder 'maps' (may be on your Desktop) and extract the content of the 2 previous zip archive into it.
You should obtain those files :

<img src="images/QtVlmMapsFiles.png" width="500" alt="Maps file structure">

Use a tool like [CyberDuck](https://cyberduck.io/) to copy those files to your PI (for example in /home/pi/Desktop).


### Weather forecast

You can download [this file](resources/20190525_105935_.grb.bz2) if you want to redo the same routing as my example.


## On windows :

[Downloads](https://www.meltemus.com/index.php/en/download/category/2-windows)
The installer need an internet connexion during the installation.
After the installation, during the first execution you will ask to download the chart (100MB). 

## On the raspberry PI :

[Downloads](https://www.meltemus.com/index.php/en/download/category/9-raspberrypi)

If your Raspberry is not connected to Internet, you should download it then transfer it with a tool like 
[CyberDuck](https://cyberduck.io/) to your Raspberry PI.

Use a file explorer and double-click on the tar archive, it will be extracted with XArchiver.

I extracted it on the Desktop. Then you can run the file named `QtVlm` (executable) in the folder `QtVlm`: 

<img src="images/QtVlmInstall.png" width="500" alt="click here">

During the first start you will have to choose : 'specifiy a map folder' or 'download'.

<img src="images/QtVlmFirstStart.png" width="300" alt="click here">
 
If your PI is online, the simplest solution 
is to download it (100Mo), if it's not the case, use to the maps you already 
upload on the PI (cf previous part).

<img src="images/QtlVlmMaps.png" width="400" alt="click here">

# Configuration

## Format for latitude/longitude
To use a format 'minutes with decimals' instead of 'seconds', go to the Menu `QtVlm > Configuration`.
Choose the format `dd°mm.mm'` 

<img src="images/QtVlmConf6.png" width="700" alt="Format">

## Grib display
I prefer to change my display, but you may prefer to keep the original configuration.
Go to menu `Grib > Grib Drawing Options`
Change the `Grib Arrows size` and the colour of 'main arrow' to black.

## Boat Configuration
We need to create a boat: go to the menu `Boat > Boat(s) Settings`:
1. Define the boat information

<img src="images/QtVlmConf1.png" width="400" alt="Boat information 1">

2. Import and configure the polar :

<img src="images/QtVlmConf2.png" width="400" alt="Boat information 2">

Now, you have to change the efficiency: the polar are defined for a racing crew with perfect sails... 
and your crew will probably be less efficient during a cruise.
You will have to find the best parameter for you. On my boat, I use 80%, 70% and 85%.

<img src="images/QtVlmConf3.png" width="400" alt="Boat inforamtion 3">

3. Motor:

Define when you will decide to use the engine and what your speed will be in this case.

<img src="images/QtVlmConf4.png" width="400" alt="Boat information 4">

4. Dimension:

<img src="images/QtVlmConf5.png" width="400" alt="Boat information 5">

Now you are ready to define your first route !

# How compute a routing:

You will have to follow this part every time you want to compute a new routing or update a weather forecast file.

## First import a weather file (with a tool like ZyGrib)

Your should have a file with a name like `20190525_105935_.grb.bz2`.
You should cover a zone large enough to compute different routes and with enough future dates.
 
You can use [this file](resources/20190525_105935_.grb.bz2) if you want to do exactly the same operation than I did on my exampe. 
 
The file should be imported with the menu `Gric > Grib slot1 > Open`.
You should see you forecast after the import :

<img src="images/QtVlmRoute1.png" width="800" alt="WeatherChart">

## Define you POI (Point of Interest)

The will be use as your arrival and start point.
Right click on the chart and create a `New Mark`. 
Example with Horta :

<img src="images/QtVlmRoute2.png" width="500" alt="New mark">
 
Define another one in the middle of the atlantic : 27° 52' N and 38° W.

<img src="images/QtVlmRoute3.png" width="500" alt="New mark">

## Create a routing with those POIs
Menu `Routings > Create a routing`
1. Uselect from boat, and select your start and arrival POI.
2. Keep starting date : use 25/05/2019 10:00 as we are using an old weather forecast file.
3. Unselect `Automatic parameters`
4. Unselect `Convert to Route...` (we'll use it later)

<img src="images/QtVlmRoute4.png" width="500" alt="Create route">

Define the same step duration (60' for isochrome).

<img src="images/QtVlmRoute5.png" width="500" alt="Create route2">

You may choose to compute alternative routes :

<img src="images/QtVlmRoute6.png" width="500" alt="Create route3">

The CPU has a huge impact on the duration fo the computation, but the output is the same.
* 17s on my Mac Book Pro (2017)
* 5 min on the Raspberry 3


<img src="images/QtVlmRoute7.png" width="800" alt="Create route4">

The best routing is the green line.
You can click everywhere in the lines, the best routing to go to this point will be displayed.

## Optional : have an idea of the 'instability' of the routing.
If you want to have this information you have to edit the routing (we need to do it in 2 phases).
Menu `Routing > Edit Routing`
On the 2nd tab select `Inversed isochrome`.

<img src="images/QtVlmRoute8.png" width="500" alt="Inversed isochrome">

Another 5 min computation is needed.

After that you will see a gray zone around the best routing.
If the band is small, it means you will have a `gate`: if you miss it, your 
real route will be very different from the predict's one.

In the previous example, we can see that we'll have some flexibility on the first third of the routing, but a lot of
incertitude for the second third : a small difference (2h hours late) should conduct to a great difference at the end (days ?).
Like the [`butterfly effect`](https://en.wikipedia.org/wiki/Butterfly_effect)

<img src="images/QtVlmRoute9.png" width="800" alt="instability of the routing">

## From routing to route

Now we want to convert this routing to a route.
Edit the routing (Menu `Routing > Edit routing`) and check the check box `Convert to Route using this prefix`.

A conversion windows will be displayed. I prefer to not simplify the route: 
a lot of point remains (sometime really noisy, but I found that the simplification is too destructive).

<img src="images/QtVlmRoute10.png" width="250" alt="create route">

The result : a route with a lot of waypoints, the whites are the day part of the route and the grays are the night part of the route.
The arrow in the circle is the direction of the wind when we'll be there (ma be in 3 days). The wind in the circle is different from the wind display with 
the Grib, because the wind of the grib is the wind a the same time on all the chart.
 
 <img src="images/QtVlmRoute11.png" width="800" alt="lot of waypoints">

## Using the route
You can edit the route (Menu `Route > Edit Route` or right click on the route) and access to other information.

<img src="images/QtVlmRoute13.png" width="250" alt="Using the route">

On tab 2 you can have access to the logbook (change the time frame to 60 minutes on the top right of the screen).
Hour by hour, you will have all the information on your boat. Including waves, wind, rains, cloud or any information your grib file may contain.

<img src="images/QtVlmRoute12.png" width="800" alt="logbook"> 

On tab 3, your can view any information through your journey 

<img src="images/QtVlmRoute12_bis.png" width="800" alt="graphics">

On tab 4, your have a summary of your route.

<img src="images/QtVlmRoute12_ter.png" width="800" alt="summary">


## Using the route into OpenCpn
Into QtVlm right click on your road and select `Copy Route` (the exchange format is kml).

<img src="images/QtVlmRoute13.png" width="250" alt="copy route">

Into OpenCpn, right click on your chart and select `Paste Route`.

<img src="images/QtVlmRoute14.png" width="800" alt="paste route">

The route is imported...

<img src="images/QtVlmRoute15.png" width="800" alt="imported into opencpn"> 

And you can also display your Grib file with the `Grib Plugin` in OpenCPN.

<img src="images/QtVlmRoute16.png" width="800" alt="with the grib information">

**Remember to have a look on the great documentation for having a deep understanding of the routing module.**
x