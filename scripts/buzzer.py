#!/usr/bin/env python
import logging
import time
import RPi.GPIO as GPIO

BuzzPin = 11    # Raspberry Pi Pin 17-GPIO 17
NbSeconds = 15  # Duration of the beeps in seconds


FORMAT = '%(asctime)-15s - %(levelname)s - %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


def setup(pin):
    logging.info("Setup on PIN : %s", BuzzPin)
    global BuzzerPin
    BuzzerPin = pin
    GPIO.setmode(GPIO.BOARD)  # Set GPIO Pin As Numbering
    GPIO.setup(BuzzerPin, GPIO.OUT)
    off()


def on():
    GPIO.output(BuzzerPin, GPIO.HIGH)


def off():
    GPIO.output(BuzzerPin, GPIO.LOW)


def beep(x):
    on()
    time.sleep(x)
    off()
    time.sleep(x)


def loop():
    try:
        for i in range(0, NbSeconds):
            beep(0.5)
    finally:
        off()


def destroy():
    logging.info("Clean GPIO")
    off()
    GPIO.cleanup()  # Release resource


if __name__ == '__main__':  # Program start from here
    setup(BuzzPin)
    try:
        loop()
    # except KeyboardInterrupt: # When 'Ctrl+C' is pressed, the child program destroy() will be executed.
    finally:
        destroy()
