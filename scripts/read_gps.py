import serial

with serial.Serial('/dev/ttyUSB0', 4800, timeout=1) as ser:
    while True:
        # x = ser.read()  # read one byte
        # s = ser.read(10)  # read up to ten bytes (timeout)
        line = ser.readline()  # read a '\n' terminated line
        print(line)
