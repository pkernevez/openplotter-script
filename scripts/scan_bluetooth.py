#!/usr/bin/python3

import logging
import os
import subprocess
import time


class DeviceInfo:
    def __init__(self, index, mac, last_seen):
        self.index = str(index)
        self.mac = mac
        self.last_seen = last_seen

    def __str__(self):
        return "{},{}".format(self.index, self.mac)


class BluetoothScanner:
    def __init__(self, macs):
        self.macs = macs
        # Reset Bluetooth interface, hci0
        # os.system("sudo hciconfig hci0 down")
        # os.system("sudo hciconfig hci0 up")

    def scan(self):
        devices = {mac: DeviceInfo(i+1, mac, time.time()) for i, mac in enumerate(self.macs)}
        while True:
            try:
                time.sleep(1)
                logging.info("reset hci0")
                os.system("sudo hciconfig hci0 reset")
                with subprocess.Popen('sudo stdbuf -oL hcitool lescan --duplicates',
                                      bufsize=0, shell=True, stdout=subprocess.PIPE,
                                      stderr=subprocess.STDOUT, ) as process:
                    line = ''
                    for char in iter(lambda: process.stdout.read(1).decode('UTF-8', 'ignore'), ''):
                        try:
                            if char == '\n':
                                mac = line.split(" ")[0]
                                if devices.__contains__(mac):
                                    now = time.time()
                                    device_info = devices[mac]
                                    delta_sec = now - device_info.last_seen
                                    # logging.info(delta)
                                    if delta_sec >= 1:
                                        device_info.last_seen = now
                                        print(device_info.__str__())
                                line = ""
                            else:
                                line += char
                        except Exception as e:
                            logging.error(
                                "An error occurs during the parsing : " + str(type(e)) + " " + str(e) + " receive line is : " + line)
                            line = ""
            except Exception as e:
                logging.error("An error occurs: " + str(type(e)) + " " + str(e), e)


if __name__ == '__main__':  # Program start from here
    try:
        # sys.stderr = sys.stdout

        logging.basicConfig(filename='/var/log/bluetoothscanner/bluetoothscanner.log', format='%(asctime)-15s %(levelname)s %(message)s',
                            level='INFO')

        scanner = BluetoothScanner(["00:1B:35:12:CB:75", "40:F3:85:90:A3:89", "00:0B:57:80:66:BD"])
        scanner.scan()
    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be executed.
        logging.info("Interrupted...")